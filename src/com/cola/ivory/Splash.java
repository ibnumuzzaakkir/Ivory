package com.cola.ivory;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;

public class Splash extends Activity {
		private int progress = 70;
		private int status = 70;
		ProgressBar progressBar;
		Handler handler = new Handler();
			   
	   public void onCreate(Bundle savedInstanceState){
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.splash);     
	      progressBar = (ProgressBar) findViewById(R.id.ProgressBar01);
	      	      progressBar.setProgress(0);
	      
	     
	      
	      new Thread(new Runnable() {
	         
	         public void run() {
	            // TODO Auto-generated method stub
	            while(status < 100){
	               status = loading();
	               handler.post(new Runnable() {
	                  
	                  public void run() {
	                     // TODO Auto-generated method stub
	                  progressBar.setProgress(status);   
	                  }
	               });
	               
	            }
	            handler.post(new Runnable() {
	               
	               public void run() {
	                  // TODO Auto-generated method stub
	                  Intent inten = new Intent(Splash.this, Menu_utama.class);
	                  startActivity(inten);
	                  
	                  finish();
	               }
	               
	            });
	         }
	         
	         public int loading(){
	            try{
	               Thread.sleep(50);
	            }catch(InterruptedException ie){
	               ie.printStackTrace();
	            }
	            return progress++;
	         }
	      }).start();
	      
	   }
}
