package com.cola.ivory;



import android.os.Bundle;

import com.cola.ivory.Guide;
import com.cola.ivory.About;
import com.cola.ivory.MainActivity;
import com.cola.ivory.Menu_utama;
import com.cola.ivory.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;



public class Menu_utama extends Activity implements OnClickListener{
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.menuutama);
        
        View Kamus = findViewById(R.id.buttonstart);
        Kamus.setOnClickListener(this);
        
        View Kamus1 = findViewById(R.id.buttonguide);
        
        Kamus1.setOnClickListener(this);
        
        View Kamus2 = findViewById(R.id.buttonabout);
        
        Kamus2.setOnClickListener(this);
                
        View Exit=findViewById(R.id.buttonexit);
        Exit.setOnClickListener(this);
    }

public void onClick(View v){
	int id = v.getId();
	if (id == R.id.buttonstart) {
		Intent MNKamus = new Intent(this, MainActivity.class);
		startActivity(MNKamus);
	} else if (id == R.id.buttonguide) {
		Intent laeganteng = new Intent(this, Guide.class);
		startActivity(laeganteng);
	} else if (id == R.id.buttonabout) {
		Intent awangganteng = new Intent(this, About.class);
		startActivity(awangganteng);
	} else if (id == R.id.buttonexit) {
		AlertDialog.Builder alertKeluar = new AlertDialog.Builder(Menu_utama.this);
		alertKeluar.setMessage("Terimakasih telah menggunakan aplikasi ini, Silahkan tekan YA untuk keluar.").setCancelable(false).setPositiveButton("Ya", new AlertDialog.OnClickListener(){
				
public void onClick(DialogInterface arg0, int arg1){
	finish();
}
}).setNegativeButton("Tidak", new AlertDialog.OnClickListener(){
    
public void onClick(DialogInterface dialog, int which){
	dialog.cancel();
}
});
		AlertDialog a=alertKeluar.create();
		a.show();
	}
}
}
   